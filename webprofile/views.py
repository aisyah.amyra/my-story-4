from django.shortcuts import render, redirect
from django.http import HttpResponse
from webprofile.models import Schedule
from datetime import datetime


# # Create your views here.                   # baru handle
# def index(request):                         # urs harus ada fungsi handling yang minimal
#  return HttpResponse ("Hello world!")       # parameter request. HttpResponse : obj response

def home(request):
    return render(request,'index.html')

def edunexp(request):
    return render(request,'EducationSkills.html')

def contact(request):
    return render(request,'contactme.html')

def skills(request):
    return render(request,'Skills.html')

def BYE(request):
    return render(request,'Bye.html') 

def form(request):
    if request.method == 'GET':
        return render(request, 'form.html')
    else: # kalo request.method == 'POST'
        NameKegiatan_baru = request.POST['Name_kegiatan']
        HariTanggal_baru = datetime.strptime(request.POST['Hari_tanggal'], '%Y-%m-%d')
        Jam_baru = request.POST['Jam']
        Tempat_baru = request.POST['Tempat']
        Kategori_baru = request.POST['Kategori']
        print(type(HariTanggal_baru))

        baru_schedule = Schedule(
            Nama_kegiatan = NameKegiatan_baru,
            Hari_tanggal = HariTanggal_baru,
            Jam = HariTanggal_baru,
            Tempat = Tempat_baru,
            Kategori = Kategori_baru
        )

        baru_schedule.save()
        return redirect('/hasilPage/')


def hasil(request):
    all_hasil = Schedule.objects.all()
    context = {'all_hasil' : all_hasil}
    return render(request,'hasil.html', context)

def scheduleDetail(request, jadwal_id):
    jadwal = Schedule.objects.get(id = jadwal_id)
    context = {'jadwal' : jadwal}
    return render(request, 'DetailJadwal.html', context)

def hapusScheduleDetail(request, hapus_jadwal_id):
    hapus = Schedule.objects.get(id = hapus_jadwal_id).delete()
    return redirect('/hasilPage/')
    






