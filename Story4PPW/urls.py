"""Story4PPW URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from webprofile import views

urlpatterns = [
    path('admin/', admin.site.urls, name= 'admin'),        #fungsi , nama path, fungsinya 
    path('', views.home, name= 'home'),              #fungsi yang dipanggil file:views, nama fungsi:index
    path('EduandExpPage/', views.edunexp, name='eduandexp'),
    path('skillsPage/', views.skills, name='skills'),
    path('contactPage/', views.contact, name='contact'),  
    path('byePage/', views.BYE, name='bye'), 
    path('formPage/',views.form, name='form'),
    path('hasilPage/',views.hasil, name='hasil'),
    path('detailPage/<int:jadwal_id>', views.scheduleDetail, name='detail'),
    path('hapusPage/<int:hapus_jadwal_id>', views.hapusScheduleDetail, name='hapus')



]
