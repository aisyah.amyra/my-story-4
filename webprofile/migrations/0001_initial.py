# Generated by Django 2.1.1 on 2019-10-04 03:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Nama_kegiatan', models.CharField(max_length=30)),
                ('Hari_tanggal', models.DateField()),
                ('Jam', models.EmailField(max_length=254)),
                ('Tempat', models.CharField(max_length=30)),
            ],
        ),
    ]
